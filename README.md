# README #

docker scripts for comon dev commands that I use

### What is this repository for? ###

* This creates executeble commands in 
    * /usr/local/bin
    * removes .sh from each file
    * uses source file ./interface/docker_interface_functions.sh

### How do I get set up? ###

* run
    * sudo ./setup-scripts.sh

Each command will be placed in /usr/local/bin with +x access
* docker-clean-all
* docker-destroy-all
* docker-destroy
* docker-rm-all
* docker-rmi-all
* docker-stop-all

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Hector Menchaca @ hm34306@hotmail.com
