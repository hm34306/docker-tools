#/bin/bash
# docker-destroy
##
## FILE: docker-stop-all.sh
##
## DESCRIPTION: Copies all scripts/* to be executable in /usr/local/bin, removing .sh
##   Each command must be run as sudo
##   
## AUTHOR: Hector Menchaca
##
## DATE: 2017-12-20
## 
## VERSION: 1.0
##
## USAGE: docker-stop-all -h
##
## RIGHTS: As is, free to use
##
# 

BIN=/usr/local/bin
SCRIPTS=$(pwd)/scripts
echo $SCRIPTS

for file in $SCRIPTS/*.sh; do
    echo $file
    file_only=${file##*/}    
    cp -- "$file" "$BIN/${file_only%%.sh}"
    chmod 755 "$BIN/${file_only%%.sh}"
done


ls -l $SCRIPTS

for file in $SCRIPTS/*.sh; do
    file_only=${file##*/}
    ls "$BIN/${file_only%%.sh}"
done

mkdir -p $BIN/interface
cp $(pwd)/scripts/interface/docker_interface_functions.sh $BIN/docker_interface_functions.sh

