#!/bin/bash
# docker_interface_functions
##
## FILE: docker_interface_functions.sh
##
## DESCRIPTION: docker_interface_functions, helper file to be sourced, continain default function and properties
##
## AUTHOR: Hector Menchaca
##
## DATE: 2017-12-20
## 
## VERSION: 1.0
##
## USAGE: .docker_interface_functions.sh
##
## RIGHTS: As is, free to use
##

# Color Properties
RED='\033[0;31m'
GREEN='\033[42m'
CYAN='\033[46m'
BLACK='\033[40m'
RESET='\033[0;39m'

# Interface Properties
COMMAND="COMMAND not set"
QUESTION="Question not set"
REQUIRES_ARGS='FALSE'
REQUIRE_ARGS_MESSAGE="REQUIRE_ARGS_MESSAGE not set"

# Response default to n for question
RESPONSE="n"

question()
{
    echo "$QUESTION"
    read accept
    RESPONSE=$accept
}

abort()
{
    echo "Aborting $COMMAND........................[${CYAN}ABORT${RESET}]"
}

pre_execute()
{
    echo "About to execute $COMMAND"
}

execute_docker_ps()
{
    echo ""
    echo "********* docker ps $2 **************"
    docker ps
    echo""
}

execute_template()
{
    pre_execute $1

    echo ""
    echo "********* $COMMAND **************"
    execute $1
    echo""

    execute_docker_ps
    post_execute $1
}

post_execute()
{
    echo "$COMMAND is complete.....................[${GREEN}OK${RESET}]"
}

root_check()
{
    is_root=$(whoami)
    if [ "$is_root" != "root" ]
    then 
        echo "Command must run with root or using sudo"
        exit
    fi
}

arg_check()
{
    if [ "$REQUIRES_ARGS" = "TRUE" ]
    then
        echo -e $REQUIRE_ARGS_MESSAGE
        exit 1
    fi
}

main()
{
    root_check

    if [ "$1" = "-h" ]
    then
        help
        exit 0
    fi

    # Check args
    arg_check

    if [ "$REQUIRES_ARGS" = "TRUE" ]
    then
        main_with_args $1 $2
    else
        main_no_args $1
    fi
}

main_no_args()
{
    
    if [ "$1" = "-y" ]
    then
        execute_template $1
    else
        question
        if [ "$RESPONSE" = "y" ]
        then
            execute_template '-y'
        else
            abort
        fi
    fi
}

main_with_args()
{
    echo $1
    if [ "$1" = "-h" ]
    then
        help
    elif [ "$1" = "-y" ]
    then
        execute_template $2
    else
        question
        if [ "$RESPONSE" = "y" ]
        then
            execute_template $1
        else
            abort
        fi
    fi
}