#/bin/bash
# docker-destroy-all
##
## FILE: docker-destroy-all.sh
##
## DESCRIPTION: docker-destroy-all stops all running docker containers and removes them
##              Command must be run as sudo
##
## AUTHOR: Hector Menchaca
##
## DATE: 2017-12-20
## 
## VERSION: 1.0
##
## USAGE: docker-destroy-all -h
##
## RIGHTS: As is, free to use
##

. docker_interface_functions.sh

COMMAND="docker-destroy-all"

# Interface Properties implementation
QUESTION="Are you sure you want to stop and remove all containers? (y/n)"

help()
{
    echo "NAME
=============================================
$COMMAND - docker tool from hm34306's toolbox

SYNOPSIS
    $COMMAND [-y]

DESCRIPTION
     $COMMAND will do 2 things:
       1. stop all docker containers
       2. remove all docker containers

OPTIONS
     -y   automatic accept

     -h   Help
     "
}

# Abstract method implementation
execute()
{
    docker-stop-all $1
    docker-rm-all $1
}

# Call main method in docker_interface_functions.sh
main $1 $2
