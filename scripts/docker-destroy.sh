#/bin/bash
# docker-destroy
##
## FILE: docker-destroy.sh
##
## DESCRIPTION: Stop and remove a given docker continer
##              Command must be run as sudo
##
## AUTHOR: Hector Menchaca
##
## DATE: 2017-12-20
## 
## VERSION: 1.0
##
## USAGE: docker-destroy -h
##
## RIGHTS: As is, free to use
##

. docker_interface_functions.sh

COMMAND="docker-destroy"

# Interface Properties implementation
QUESTION="Are you sure you want to stop and remove $1? (y/n)"
REQUIRES_ARGS='TRUE'
REQUIRE_ARGS_MESSAGE="No arguments provided. Docker container name/id required.....[${RED}ERROR${RESET}]"

help()
{
    echo "NAME
=============================================
$COMMAND - docker tool from hm34306's toolbox

SYNOPSIS
    $COMMAND [-y]

DESCRIPTION
     $COMMAND will do 1 thing:
       1. stop named docker container
       2. remove named docker container

OPTIONS
     -y   automatic accept

     -h   Help
     "
}

# Abstract method implementation
execute()
{
    docker stop $1
    docker rm $1
}

# Call main method in docker_interface_functions.sh
main $1 $2
