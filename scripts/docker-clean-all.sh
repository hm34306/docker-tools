#/bin/bash
# docker-clean-all
##
## FILE: docker-clean-all.sh
##
## DESCRIPTION: docker-clean-all stops all running containers and removes them, and removes all images
##              Command must be run as sudo
##
## AUTHOR: Hector Menchaca
##
## DATE: 2017-12-20
## 
## VERSION: 1.0
##
## USAGE: docker-clean-all -h
##
## RIGHTS: As is, free to use
##

. docker_interface_functions.sh

COMMAND="docker-destroy-all"

# Interface Properties implementation
QUESTION="Are you sure you want to stop and stop and remove all containers\nAs well as purge all images? (y/n)"

help()
{
    echo "NAME
=============================================
$COMMAND - docker tool from hm34306's toolbox

SYNOPSIS
    $COMMAND [-y]

DESCRIPTION
     $COMMAND will do 3 things:
       1. Stop all docker containers
       2. Remove all docker containers
       3. Remove all docker images
       
OPTIONS
     -y   automatic accept

     -h   Help
     "
}

# Abstract method implementation
execute()
{
    docker-stop-all $1
    docker-rm-all $1
    docker-rmi-all $1
}

# Call main method in docker_interface_functions.sh
main $1
