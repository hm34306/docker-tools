#/bin/bash
# docker-destroy
##
## FILE: docker-rmi-all.sh
##
## DESCRIPTION: Remove all docker images
##              Command must be run as sudo
##
## AUTHOR: Hector Menchaca
##
## DATE: 2017-12-20
## 
## VERSION: 1.0
##
## USAGE: docker-rmi-all -h
##
## RIGHTS: As is, free to use
##

. docker_interface_functions.sh

COMMAND="docker-rmi-all"

# Interface Properties implementation
QUESTION="Are you sure you want to remove all images? (y/n)"

help()
{
    echo "NAME
=============================================
$COMMAND - docker tool from hm34306's toolbox

SYNOPSIS
    $COMMAND [-y]

DESCRIPTION
     $COMMAND will do 1 thing:
       1. remove all images

OPTIONS
     -y   automatic accept

     -h   Help
     "
}

# Abstract method implementation
execute()
{
    docker rmi -f $(docker images -a -q)
}

# Call main method in docker_interface_functions.sh
main $1
