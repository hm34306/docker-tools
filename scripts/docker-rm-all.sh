#/bin/bash
# docker-rm-all
##
## FILE: docker-rm-all.sh
##
## DESCRIPTION: Stop all container
##              Command must be run as sudo
##
## AUTHOR: Hector Menchaca
##
## DATE: 2017-12-20
## 
## VERSION: 1.0
##
## USAGE: docker-rm-all -h
##
## RIGHTS: As is, free to use
##

. docker_interface_functions.sh

COMMAND="docker-rm-all"

# Interface Properties implementation
QUESTION="Are you sure you want to remove all containers? (y/n)"

help()
{
    echo "NAME
=============================================
$COMMAND - docker tool from hm34306's toolbox

SYNOPSIS
    $COMMAND [-y]

DESCRIPTION
     $COMMAND will do 2 things:
       1. remove all containers

OPTIONS
     -y   automatic accept

     -h   Help
     "
}

# Abstract method implementation
execute()
{
    docker rm $(docker ps -a -q)
}

# Call main method in docker_interface_functions.sh
main $1

