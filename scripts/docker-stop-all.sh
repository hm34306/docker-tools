#/bin/bash
# docker-destroy
##
## FILE: docker-stop-all.sh
##
## DESCRIPTION: Stop all docker containers
##              Command must be run as sudo
##
## AUTHOR: Hector Menchaca
##
## DATE: 2017-12-20
## 
## VERSION: 1.0
##
## USAGE: docker-stop-all -h
##
## RIGHTS: As is, free to use
##

. docker_interface_functions.sh

COMMAND="docker-stop-all"

# Interface Properties implementation
QUESTION="Are you sure you want to stop all containers? (y/n)"

help()
{
    echo "NAME
=============================================
$COMMAND - docker tool from hm34306's toolbox

SYNOPSIS
    $COMMAND [-y]

DESCRIPTION
     $COMMAND will do 1 thing:
       1. Stop all containers

OPTIONS
     -y   automatic accept

     -h   Help
     "
}

# Abstract method implementation
execute()
{
    docker stop $(docker ps -a -q)
}

# Call main method in docker_interface_functions.sh
main $1

